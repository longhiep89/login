<!DOCTYPE html>
<html>

<head>
    <title>tangthilonghiep.toidayhoc.com</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <a href="index.php?page=register" class="btn btn-success">'Đăng ký'</a>
            <a href="index.php" class="btn btn-info">'Đăng nhập'</a>
            <a href="index.php" class="btn btn-success">'RESET'</a>
            <?php if(isset($_SESSION["loged"])) echo "<a href='index.php?act=logout' class='btn btn-danger'>Đăng xuất</a>"; ?>
        </div>

        <div class="row">
            <!-- 'start nếu xảy ra lỗi thì hiện thông báo:' -->
            <?php
				if(isset($_COOKIE["error"])){?>
            <div class="alert alert-danger">
                <strong>'Có lỗi!'</strong> <?php echo $_COOKIE["error"]; ?>
            </div>
            <?php } ?>
            <!-- 'end nếu xảy ra lỗi thì hiện thông báo:' -->


            <!-- 'start nếu thành công thì hiện thông báo:' -->
            <?php
				if(isset($_COOKIE["success"])){
			?>
            <div class="alert alert-success">
                <strong>'Chúc mừng!'</strong> <?php echo $_COOKIE["success"]; ?>
            </div>
            <?php } ?>
            <!-- 'end nếu thành công thì hiện thông báo:' -->
            <div>
                <?php
			// nếu tồn tại biến $_GET["page"] = "register" thì gọi trang đăng ký:
			if(isset($_GET["page"])&&$_GET["page"]=="register")
				include "register.php";


			//nếu không tồn tại biến $_GET["page"] = "register"
			if(!isset($_GET["page"])){
				// nếu tồn tại biến session $_SESSION["loged"] thì gọi nội dung trang admin.php vào
				if(isset($_SESSION["loged"]))
					include "admin.php";
				//nếu không tồn tại biến session $_SESSION["loged"] thì gọi nội dung trang login.php vào
				else
					include "login.php";
			}
			?>

            </div>

			
</body>

</html>